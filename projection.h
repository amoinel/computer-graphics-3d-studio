#include <iostream>

#include <glm/vec3.hpp> // glm::vec3
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/gtc/type_ptr.hpp>

// Class for projection specification
class Projection
{
public:
    Projection();
    ~Projection();

    /**
    * Sets the matrix projection.
    * @param matProjection The matrix projection.
    */
    void setProjectionMatrix(glm::mat4 matProjection);

    /**
    * Returns the matrix projection.
    * @return The matrix projection.
    */
    glm::mat4& getProjectionMatrix();

    /**
    * Initiates the parallel projection
    * @param top The top of the far plane.
    * @param farplane The distance of the far plane.
    * @param obliqueScale The oblique scale.
    * @param obliqueAngleRad The oblique angle in radians.
    * @param ratio The ratio between the width and the height.
    * 
    */
    void parallel(const float top, 
                  const float farplane, 
                  const float obliqueScale, 
                  const float obliqueAngleRad,
                  const float ratio);

    /**
    * Initiates the perspective projection
    * @param fov The field of view.
    * @param far
    * @param ratio The oblique scale.
    * 
    */
    void perspective(const float fov, 
                     const float far, 
                     const float ratio);

private:
    glm::vec3 fov;
    glm::mat4 matProjection;
};