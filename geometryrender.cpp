/*
 *  Workshop 1
 *  Computer Graphics course
 *  Dept Computing Science, Umea University
 *  Stefan Johansson, stefanj@cs.umu.se
 */

#define GLM_ENABLE_EXPERIMENTAL
#include "geometryrender.h"
#include <math.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/vec3.hpp> // glm::vec3
#include <glm/ext/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale
#include "glm/gtx/string_cast.hpp"
#include "./SOIL2.h"

using namespace std;

// Initialize OpenGL
void GeometryRender::initialize()
{
    // Enable depth test
    glEnable(GL_DEPTH_TEST);

    // Create and initialize a program object with shaders
    program = initProgram("vshader.glsl", "fshader.glsl");
    // Installs the program object as part of current rendering state
    glUseProgram(program);

    // Creat a vertex array object
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    // Create vertex buffer in the shared display list space and
    // bind it as VertexBuffer (GL_ARRAY_BUFFER)
    glGenBuffers(1, &vBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vBuffer);

    /* Create buffer in the shared display list space and 
       bind it as GL_ELEMENT_ARRAY_BUFFER */
    glGenBuffers(1, &iBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iBuffer);

    initializeTexture();

    // Get locations of the attributes in the shader
    locVertices = glGetAttribLocation(program, "vPosition");
    locTex = glGetAttribLocation(program, "TexCoord");
    locNormals = glGetAttribLocation(program, "vNormal");
    locModel = glGetUniformLocation(program,"M");
    locView = glGetUniformLocation(program,"V");
    locProjection = glGetUniformLocation(program,"P");

    // Get locations of the attributes (light) in the shader
    locAmbientR = glGetUniformLocation(program, "ambientR");
    locDiffuseR = glGetUniformLocation(program, "diffuseR");
    locSpecularR = glGetUniformLocation(program, "specularR");
    locShininess = glGetUniformLocation(program, "shininess");
    locApplyTex = glGetUniformLocation(program, "applyTex");
    locLightPos = glGetUniformLocation(program, "lightPos");
    locViewerPos = glGetUniformLocation(program, "viewerPos");
    locLightColor = glGetUniformLocation(program, "lightColor");
    locAmbientColor = glGetUniformLocation(program, "ambientColor");

    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    glBindVertexArray(0);
    glUseProgram(0);

    loadGeometry();

}

void GeometryRender::initializeTexture(void) {
    /* Create Buffer for texture */
    glGenTextures(1, &idTexture);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, idTexture);

    // Wrapping
    glTexParameteri(GL_TEXTURE_2D , GL_TEXTURE_WRAP_S , GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D , GL_TEXTURE_WRAP_T , GL_REPEAT);

    // Filtering
    glTexParameteri(GL_TEXTURE_2D , GL_TEXTURE_MIN_FILTER , GL_LINEAR );
    glTexParameteri(GL_TEXTURE_2D , GL_TEXTURE_MAG_FILTER , GL_LINEAR );

    // Generate mipmaps
    glGenerateMipmap(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D , 0);
}

void GeometryRender::loadFileGeometry(ObjReader &reader)
{   
    // Load the object file
    if (reader.loadOBJ()) {

        // Reinitialize values
        vertices.clear();
        indices.clear();
        normals.clear();
        texCoords.clear();
        matModel = glm::mat4(1.0f);
        reloadMatrix();

        Object* object = reader.getObject();

        indices = object->getOutIndices();
        vertices = object->getOutVertices();
        normals = object->getOutNormals();
        texCoords = object->getTexCoords();

        glUseProgram(program);
        glBindVertexArray(vao);

        setGlBuffers();
    }
}

void GeometryRender::loadGeometry(void)
{   
    
    // Define vertices in array
    vertices.push_back(normalize(glm::vec3(-2.0f, -2.0f, -2.0f)));
    vertices.push_back(normalize(glm::vec3( 2.0f,  -2.0f, -2.0f)));
    vertices.push_back(normalize(glm::vec3( 2.0f, -2.0f, 2.0f)));

    vertices.push_back(normalize(glm::vec3(-2.0f, -2.0f, -2.0f)));
    vertices.push_back(normalize(glm::vec3( 2.0f,  -2.0f, 2.0f)));
    vertices.push_back(normalize(glm::vec3( -2.0f, -2.0f, 2.0f)));

    indices.push_back(0);
    indices.push_back(1);
    indices.push_back(2);

    indices.push_back(3);
    indices.push_back(4);
    indices.push_back(5);

    glUseProgram(program);
    glBindVertexArray(vao);

    glUniformMatrix4fv(locModel, 1, GL_FALSE, glm::value_ptr(matModel));
    glUniformMatrix4fv(locView, 1, GL_FALSE, glm::value_ptr(matView));
    glUniformMatrix4fv(locProjection, 1, GL_FALSE, glm::value_ptr(matProjection));
    glUniform3fv(locAmbientR, 1, glm::value_ptr(lAmbient));
    glUniform3fv(locDiffuseR, 1, glm::value_ptr(lDiffuse));
    glUniform3fv(locSpecularR, 1, glm::value_ptr(lSpecular));
    glUniform1f(locShininess, lShininess);
    glUniform1f(locApplyTex, lApplyTex);

    glUniform3fv(locViewerPos, 1, glm::value_ptr(lViewerPos));
    glUniform3fv(locLightPos, 1, glm::value_ptr(lLightPos));
    glUniform3fv(locLightColor, 1, glm::value_ptr(lLightColor));
    glUniform3fv(locAmbientColor, 1, glm::value_ptr(lAmbientColor));
    
    setGlBuffers();
    
}

void GeometryRender::setGlBuffers(void)
{  
    // Load object data to the array buffer and index array
    size_t vSize = vertices.size()*sizeof(glm::vec3);
    size_t iSize = indices.size()*sizeof(unsigned int);
    size_t nSize = normals.size()*sizeof(glm::vec3);
    size_t tSize = texCoords.size()*sizeof(glm::vec2);

    glBufferData( GL_ARRAY_BUFFER, vSize + nSize + tSize, NULL, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, vSize, vertices.data()); 
    glBufferSubData(GL_ARRAY_BUFFER, vSize, nSize, normals.data());
    glBufferSubData(GL_ARRAY_BUFFER, vSize + nSize, tSize, texCoords.data());

    glBufferData( GL_ELEMENT_ARRAY_BUFFER, iSize, indices.data(), GL_STATIC_DRAW);
    
    // Set the pointers of locVertices and locNormals to the right places
    glVertexAttribPointer(locVertices, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
    glEnableVertexAttribArray(locVertices);
    glVertexAttribPointer(locNormals, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(vSize)); 
    glEnableVertexAttribArray(locNormals);
    glVertexAttribPointer(locTex, 2, GL_FLOAT , GL_FALSE , 0, BUFFER_OFFSET(vSize + nSize));
    glEnableVertexAttribArray(locTex);

    glBindVertexArray(0);
    glUseProgram(0);
}


// Check if any error has been reported from the shader
void GeometryRender::debugShader(void) const
{
    GLint  logSize;
    glGetProgramiv( program, GL_INFO_LOG_LENGTH, &logSize );
    if (logSize > 0) {
        std::cerr << "Failure in shader "  << std::endl;
        char logMsg[logSize+1];
        glGetProgramInfoLog( program, logSize, nullptr, &(logMsg[0]) );
        std::cerr << "Shader info log: " << logMsg << std::endl;
    }

}

// Render object
void GeometryRender::display()
{
    glUseProgram(program);
    glBindVertexArray(vao);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Bind texture to the next render call
    glBindTexture(GL_TEXTURE_2D , idTexture);

    // Call OpenGL to draw the triangle
    glDrawElements(GL_TRIANGLES, static_cast<int>(indices.size()), GL_UNSIGNED_INT, BUFFER_OFFSET(0));

    // Not to be called in release...
    debugShader();

    glBindVertexArray(0);
    glUseProgram(0);

}

void GeometryRender::_translate(float dx, float dy, float dz)
{
    matModel = translate(matModel, glm::vec3(dx, dy, dz));
    reloadMatrix();
}

void GeometryRender::_translateCamera(float dx, float dy, float dz)
{
    camera.translateCamera(dx, dy, dz);
    reloadMatrix();
}

void GeometryRender::_rotateCamera(float theta, float x, float y, float z)
{
    camera.rotateCamera(theta, x, y, z);
    reloadMatrix();
}

void GeometryRender::_scale(float x, float y, float z)
{
    matModel = scale(matModel, glm::vec3(x, y, z));
    reloadMatrix();
}

void GeometryRender::_rotate(float theta, float x, float y, float z)
{
    matModel = rotate(matModel, glm::radians(theta), glm::vec3(x, y, z));
    reloadMatrix();
}

void GeometryRender::reloadMatrix()
{
    matView = camera.getViewMatrix();
    matProjection = projection.getProjectionMatrix();
    lViewerPos = camera.getPosition();

    glUseProgram(program);
    glUniformMatrix4fv(locModel, 1, GL_FALSE, glm::value_ptr(matModel));
    glUniformMatrix4fv(locView, 1, GL_FALSE, glm::value_ptr(matView));
    glUniformMatrix4fv(locProjection, 1, GL_FALSE, glm::value_ptr(matProjection));
    glUniform3fv(locViewerPos, 1, glm::value_ptr(lViewerPos));
    glUniform1f(locApplyTex, lApplyTex);
    glUseProgram(0);
}

void GeometryRender::updateHeightWidth(const int height, const int width) {
    this->heightWindow = height;
    this->widthWindow = width;
}


void GeometryRender::updateProjectionPerspective(const float fov, const float far) {
    projection.perspective(fov, far, static_cast<float>(widthWindow) / static_cast<float>(heightWindow));
    reloadMatrix();
}

void GeometryRender::updateProjectionParallel(const float top, 
                                             const float farplane, 
                                             const float obliqueScale, 
                                             const float obliqueAngleRad) {

    float width = static_cast<float>(widthWindow);
    float height = static_cast<float>(heightWindow);

    projection.parallel(top, farplane, obliqueScale, obliqueAngleRad, width/height);
    reloadMatrix();                                             
}

void GeometryRender::updateMaterialCoefficient(const float* materialAmbient, 
                                            const float* materialDiffuse, 
                                            const float* materialSpecular,
                                            float materialShininess) {
    lAmbient = glm::make_vec3(materialAmbient);
    lDiffuse = glm::make_vec3(materialDiffuse);
    lSpecular = glm::make_vec3(materialSpecular);
    lShininess = materialShininess;

    glUseProgram(program);
    glUniform3fv(locAmbientR, 1, glm::value_ptr(lAmbient));
    glUniform3fv(locDiffuseR, 1, glm::value_ptr(lDiffuse));
    glUniform3fv(locSpecularR, 1, glm::value_ptr(lSpecular));
    glUniform1f(locShininess, lShininess);
    glUniform1f(locApplyTex, lApplyTex);
    glUseProgram(0);
};

void GeometryRender::updateLightCoefficient(const float* lightPos,
                                            const float* lightColor, 
                                            const float* ambientColor) {
    lLightPos = glm::make_vec3(lightPos);
    lLightColor = glm::make_vec3(lightColor);
    lAmbientColor = glm::make_vec3(ambientColor);

    glUseProgram(program);
    glUniform3fv(locLightPos, 1, glm::value_ptr(lLightPos));
    glUniform3fv(locLightColor, 1, glm::value_ptr(lLightColor));
    glUniform3fv(locAmbientColor, 1, glm::value_ptr(lAmbientColor));
    glUniform1f(locApplyTex, lApplyTex);
    glUseProgram(0);
}

void GeometryRender::applyTexture(const bool textureShow) {
    if (textureShow) {
        lApplyTex = 1.0f;
    } else {
        lApplyTex = 0.0f;
    }
    glUseProgram(program);
    glUniform1f(locApplyTex, lApplyTex);
    glUseProgram(0);
};

void GeometryRender::loadTexture(const char* texturePath) {
    // Load texture image
    idTexture = SOIL_load_OGL_texture
    (
        texturePath,
        SOIL_LOAD_AUTO,
        SOIL_CREATE_NEW_ID,
        SOIL_FLAG_MIPMAPS | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT
    );
};
