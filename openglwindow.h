/*
 *  Workshop 1
 *  Computer Graphics course
 *  Dept Computing Science, Umea University
 *  Stefan Johansson, stefanj@cs.umu.se
 */
#pragma once

#include "imgui.h"
#include "ImGuiFileDialog.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "3dstudio.h"

const float pi_f = 3.1415926f;

#include "objreader.h"

class OpenGLWindow
{
public:
    OpenGLWindow(std::string title, int width, int height);
    ~OpenGLWindow();

    GLFWwindow* window() const;
    virtual void errorCallback(int error, const char* desc);

    /**
    * Callback when a resize of the window is detected.
    * @param window The current window.
    * @param width The width of the window.
    * @param height The height of the window.
    */
    virtual void resizeCallback(GLFWwindow* window, int width, int height);
    virtual void keysCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
    virtual void mousePosCallback(GLFWwindow* window, double xpos, double ypos);

    void start();
    /**
    * Initializes OpenGL
    *
    */
    virtual void initialize() = 0;

    /**
    * Translates the model matrix.
    *
    * @param dx Unit(s) of translation on the x axis.
    * @param dy Unit(s) of translation on the y axis.
    * @param dz Unit(s) of translation on the z axis.
    */
    virtual void _translate(float dx, float dy, float dz) = 0;

    /**
    * Translates the view matrix.
    *
    * @param dx Unit(s) of translation on the x axis.
    * @param dy Unit(s) of translation on the y axis.
    * @param dz Unit(s) of translation on the z axis.
    */
    virtual void _translateCamera(float dx, float dy, float dz) = 0;

    /**
    * Rotates the view matrix.
    *
    * @param theta Degree of rotation.
    * @param x
    * @param y
    * @param z
    */
    virtual void _rotateCamera(float theta, float x, float y, float z) = 0;

    /**
    * Scales the model matrix.
    *
    * @param x Ratio of scale on the x axis.
    * @param y Ratio of scale on the y axis.
    * @param z Ratio of scale on the z axis.
    */
    virtual void _scale(float x, float y, float z) = 0;

    /**
    * Rotates the model matrix.
    *
    * @param theta Degree of rotation.
    * @param x
    * @param y
    * @param z
    */
    virtual void _rotate(float theta, float x, float y, float z) = 0;

    /**
    * Modifies value of uniform variable of the MVP matrix.
    *
    */
    virtual void reloadMatrix(void) = 0;

    /**
    * Reads an OBJ file in order to load it.
    * @param reader instance of the object reqder file.
    */
    virtual void loadFileGeometry(ObjReader &reader) = 0;

    /**
    * Updates the perspective projection when values are updated from th GUI.
    * @param fov Field of view.
    * @param far Far cliping.
    */
    virtual void updateProjectionPerspective(const float fov, const float far) = 0;
    
    /**
    * Updates the parallel projection when values are updated from th GUI.
    * @param top
    * @param farplane
    * @param obliqueScale
    * @param obliqueAngleRad
    */
    virtual void updateProjectionParallel(const float top, 
                                             const float farplane, 
                                             const float obliqueScale, 
                                             const float obliqueAngleRad) = 0;
    
    /**
    * Updates the material coefficient when values are updated from th GUI.
    * @param materialAmbient
    * @param materialDiffuse
    * @param materialSpecular
    * @param materialShininess
    */
    virtual void updateMaterialCoefficient(const float* materialAmbient,
                                           const float* materialDiffuse,
                                           const float* materialSpecular,
                                           float materialShininess) = 0;
    
    /**
    * Updates the light coefficient when values are updated from th GUI.
    * @param lightPos
    * @param lightColor
    * @param ambientColor
    */
    virtual void updateLightCoefficient(const float* lightPos,
                                        const float* lightColor,
                                        const float* ambientColor) = 0;
    
    /**
    * Updates the value of the height nd width of the window.
    * @param height The height of the window.
    * @param width The width of the window.
    */
    virtual void updateHeightWidth(const int height,
                                   const int width) = 0;

    /**
    * Loads the texture in memory when chose from the GUI.
    * @param texturePath The file path of the texture.
    */
    virtual void loadTexture(const char* texturePath) = 0;

    /**
    * Applies or not the texture load in memory when chosen.
    * @param textureShow A boolean use to indicate if the texture must be apply or not.
    */
    virtual void applyTexture(const bool textureShow) = 0;

    /**
    * Displays the different objects on the scene.
    */
    virtual void display() = 0;
    void displayNow();

    /**
    * Draws the global GUI
    *
    */
    void DrawGui();

protected:
// OpenGL error handler
    /**
    * Displays and returns a boolean that indicates if
    * errorshave been handle in opengl.
    * @return the height width.
    */
    bool checkOpenGLError() const;

    /**
    * Returns the window width.
    * @return the window width.
    */
    int width() const;

    /**
    * Returns the height width.
    * @return the height width.
    */
    int height() const;

    std::string readShaderSource(const std::string shaderFile) const;
    GLuint initProgram(const std::string vShaderFile, const std::string fShaderFile) const;

    void reshape(const int width, const int height);

private:
    GLFWwindow* glfwWindow;
    int windowWidth = 0;
    int windowHeight = 0;
    std::string objFileName;
    std::string objFilePath;
    float fov;
    float farplane;
    float top;
    float obliqueScale;
    float obliqueAngleRad;
    int proj_current_idx = 0;

    float lastTime;
    float currentTime;
    float deltaTime;
    float horizontalAngle = 0;
    float verticalAngle = 0;
    float lastXpos = 0;
    float lastYpos = 0;

    //Light
    float materialAmbient[3] = {.5f, .5f, .5f};
    float materialDiffuse[3] = {.5f, .5f, .5f};
    float materialSpecular[3] = {.5f, .5f, .5f};
    float materialShininess = 1.0f;

    float lightPos[3] = {0.0f, 0.0f, 0.0f};
    float lightColor[3] = {1.0f, 1.0f, 1.0f};
    float ambientColor[3] = {0.2f, 0.2f, 0.2f};

    //Texture
    std::string textureFileName;
    std::string textureFilePath;
    bool textureShow = false;
};
