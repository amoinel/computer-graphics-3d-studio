#define GLM_ENABLE_EXPERIMENTAL
#include <glm/ext/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale
#include <glm/gtc/type_ptr.hpp>
#include "glm/gtx/string_cast.hpp"

#include "projection.h"

Projection::Projection()
{
    matProjection = glm::perspective(glm::radians(60.0f), 
                                               4.0f / 3.0f, 
                                               0.1f,
                                               100.f);
}

Projection::~Projection()
{}

glm::mat4& Projection::getProjectionMatrix()
{
    return this->matProjection;
}

void Projection::setProjectionMatrix(glm::mat4 matProjection)
{
    this->matProjection = matProjection;
}

void Projection::perspective(const float fov, const float far, const float ratio) {
    matProjection = glm::perspective(glm::radians(fov), ratio, 0.1f, far);
}

void Projection::parallel(const float top, 
                  const float farplane, 
                  const float obliqueScale, 
                  const float obliqueAngleRad,
                  const float ratio) {

    float left = -top * ratio;
    float right = top * ratio;
    float bottom = -top;
    float near = 0.1f;

    //ST
    glm::mat4 Porth = glm::ortho(left, right, bottom, top, near, farplane);

    glm::mat4 H = glm::mat4(            1,                                          0,                          0, 0,
                                        0,                                          1,                          0, 0, 
                            obliqueScale * std::cos(obliqueAngleRad), obliqueScale * std::sin(obliqueAngleRad), 1, 0,
                                        0,                                          0,                          0, 1);

    this->matProjection = Porth * H;
}