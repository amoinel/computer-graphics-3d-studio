#define GLM_ENABLE_EXPERIMENTAL
#include <glm/ext/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale
#include <glm/gtc/type_ptr.hpp> // glm::inverse
#include <glm/gtx/rotate_vector.hpp>

#include "camera.h"

Camera::Camera()
{
    this->position = glm::vec3(0, 0, 2);
    this->target = glm::vec3(0, 0, 0);
    this->up = glm::vec3(0, 1, 0),

    updateViewMatrix();
}

Camera::~Camera()
{}

glm::vec3 Camera::getPosition()
{
    return this->position;
}

glm::vec3 Camera::getTarget()
{
    return this->target;
}

glm::vec3 Camera::getUp()
{
    return this->up;
}

glm::mat4 Camera::getViewMatrix()
{
    return this->matView;
}

void Camera::setPosition(glm::vec3 position1)
{
    this->position = position1;
}

void Camera::setTarget(glm::vec3 target1)
{
    this->target = target1;
}

void Camera::setUp(glm::vec3 up1)
{
    this->up = up1;
}

void Camera::setViewMatrix(glm::mat4 viewMatrix)
{
    this->matView = viewMatrix;
}

void Camera::updateViewMatrix()
{
    this->matView = glm::lookAtRH(
                this->position,
                this->target,
                this->up
    );
}

void Camera::rotateCamera(float theta, const float x, const float y, const float z) {
    glm::vec3 dirVec = getTarget() - getPosition();
    glm::mat3 rotMat = glm::mat3(1.0f);
    rotMat = rotate(glm::mat4(1.0f), glm::radians(theta), glm::vec3(x, y, z));
    setTarget(getPosition() + (rotMat * dirVec));
    updateViewMatrix();
}

void Camera::translateCamera(float dx, float dy, float dz) {

    glm::mat4 invertMatView = glm::inverse(getViewMatrix());
    glm::vec4 p0 = invertMatView * glm::vec4(dx, dy, dz, 1.0f);
    glm::vec4 pref = invertMatView * glm::vec4(dx, dy, dz-2.0f, 1.0f);
    setPosition(glm::vec3(p0));
    setTarget(glm::vec3(pref));
    updateViewMatrix();
}