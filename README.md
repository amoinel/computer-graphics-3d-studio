# Computer graphics 3D Studio

 A limited 3D visualization software.

## Usage

Go to the root directory of the project.

To compile the project:

```bash
## Command make realises a "make clean" and a "make"
make
```

Then to start the project:

```bash
./project
```

## Commands
**You can triggered some commands on the objects of the scene:**

Keyboard commands:
 - L: Translate object the x-axis (+)
 - J: Translate object the x-axis (-)
 - I: Translate object the y-axis (+)
 - K: Translate object the y-axis (-)

 - P: Scale up
 - M: Scale down

 - UP: Rotate the object on the x-axis (+10°)
 - DOWN: Rotate the object on the x-axis (-10°)
 - RIGHT: Rotate the object on the y-axis (+10°)
 - LEFT: Rotate the object on the y-axis (-10°)
 - CTRL+UP: Rotate the object on the z-axis (+10°)
 - CTRL+DOWN: Rotate the object on the z-axis (-10°)

 - E: Translate camera on the x-axis (+)
 - Q: Translate camera on the x-axis (-)
 - D: Translate camera on the y-axis (+)
 - A: Translate camera on the y-axis (-)
 - S: Translate camera on the z-axis (+)
 - W: Translate camera on the z-axis (-)

Mouse commands:
- Middle Button + Movement: Rotate point around the x-axis/y-axis:


