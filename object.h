#include <iostream>
#include <vector>

#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec2.hpp> // glm::vec3
#include <glm/gtc/type_ptr.hpp>

// Class for save object specification
class Object
{
public:
    Object();
    ~Object();

    
    std::vector<unsigned int>&  getOutIndices();
    std::vector <glm::vec3>& getOutVertices();
    std::vector <glm::vec3>& getOutNormals();
    std::vector <glm::vec2>& getTexCoords();

    void setOutIndices(std::vector<unsigned int>* outIndices);
    void setOutVertices(std::vector <glm::vec3>* outVertices);
    void setOutNormals(std::vector <glm::vec3>* outNormals);
    void populateTexCoord();

private:
    std::vector <unsigned int> outIndices;
    std::vector <glm::vec3> outVertices;
    std::vector <glm::vec2> outUvs;
    std::vector <glm::vec3> outNormals;
    std::vector <glm::vec2> texCoords;
    std::vector <unsigned int> vertexIndices, uvIndices, normalIndices;
};