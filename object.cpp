#include "object.h"
const float pi_f = 3.1415926f;

Object::Object()
{}

Object::~Object()
{}

std::vector<unsigned int>& Object::getOutIndices()
{
    return outIndices;
}

std::vector<glm::vec3>& Object::getOutVertices()
{
    return outVertices;
}

std::vector<glm::vec3>& Object::getOutNormals()
{
    return outNormals;
}

std::vector<glm::vec2>& Object::getTexCoords()
{
    populateTexCoord();
    return texCoords;
}

void Object::setOutIndices(std::vector<unsigned int>* outIndices)
{
    this->outIndices = *outIndices;
}

void Object::setOutVertices(std::vector<glm::vec3>* outVertices)
{
    this->outVertices = *outVertices;
}

void Object::setOutNormals(std::vector<glm::vec3>* outNormals)
{
    this->outNormals = *outNormals;
}

void Object::populateTexCoord()
{
    glm::vec3 intersectionP;

    float radius = 1.0f;
    float a = 1.0f;
    float b = 0.0f;
    float c = 0.0f;
    float s = 0.0f;
    float t = 0.0f;
    float delta = 0.0f;
    float dpos = 0.0f;

    for (unsigned int i = 0; i < getOutVertices().size(); i++) {
        a = glm::dot(getOutNormals()[i], getOutNormals()[i]);
        b = 2*(glm::dot(getOutVertices()[i], getOutNormals()[i]));
        c = (glm::dot(getOutVertices()[i], getOutVertices()[i]) - pow(radius, 2));
        delta = pow(b, 2) - 4*a*c;

        if (delta > 0) {
            dpos = std::max((-b - sqrt(delta)) / (2 * a), (-b + sqrt(delta)) / (2 * a));
            intersectionP = getOutVertices()[i] + dpos*getOutNormals()[i];
            intersectionP = glm::normalize(intersectionP)*radius;

            s = acos(intersectionP.x/radius)/pi_f;
            t = ((atan2(intersectionP.z, intersectionP.y) / (2 * pi_f)) + 0.5);
            texCoords.push_back(glm::vec2(s, t));
        }
    }
}