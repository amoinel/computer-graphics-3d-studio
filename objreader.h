#include <iostream>
#include <vector>

#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec2.hpp> // glm::vec3

#include "object.h"

// Class for read and load file object
class ObjReader
{
public:
    ObjReader(const char *filePath, Object *object);
    ~ObjReader();

    /**
    * Loads the object in memory from the OBJ file
    * @return A boolean that indicates whether the file havec been charged or no.
    */
    bool loadOBJ();

    /**
    * Returns the matrix projection.
    * @return The created object from OBJ file.
    */
    Object* getObject();


private:
    /*The read file*/
    FILE * file;

    /*The object charged from the OBJ file*/
    Object *object;


    /**
    * Returns the matrix projection.
    * @param str String to tokenize.
    * @param delimiter Character using to delimite the tokens.
    * @param out The vector who contains the tokens obtain.
    * 
    */
    void tokenize(std::string const &str, const char delimiter, std::vector<std::string> &out);
};