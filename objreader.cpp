#define GLM_ENABLE_EXPERIMENTAL
#include <cstring>
#include <sstream>
#include <algorithm>
#include <glm/glm.hpp>
#include <glm/gtx/normal.hpp>
#include "objreader.h"

ObjReader::ObjReader(const char *filePath, Object *object)
{
    file = fopen(filePath, "r");
    this->object = object;
}

ObjReader::~ObjReader()
{}

bool ObjReader::loadOBJ()
{
    std::vector <glm::vec3> temp_vertices;
    std::vector <glm::vec2> temp_uvs;
    std::vector <glm::vec3> temp_normals;
    std::vector <unsigned int> vertexIndices, uvIndices, normalIndices;

    float maxPoint = 0.0f;

    if( file == NULL ){
        printf("Impossible to open the file !\n");
        return false;
    }

    while (true) {

        char lineHeader[128];

        int res = fscanf(file, "%s", lineHeader);
        if (res == EOF)
            break;

        if (strcmp(lineHeader, "v") == 0) {
            glm::vec3 vertex(3);
            fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z );
            temp_vertices.push_back(vertex);

            // Max Point computes
            maxPoint = std::max(maxPoint, std::max(std::abs(vertex.x), std::max(std::abs(vertex.y), std::abs(vertex.z))));

        } else if (strcmp(lineHeader, "vt") == 0) {
            glm::vec2 uv(2);
            fscanf(file, "%f %f\n", &uv.x, &uv.y );
            temp_uvs.push_back(uv);

        } else if (strcmp(lineHeader, "vn") == 0) {
            glm::vec3 normal(3);
            fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
            temp_normals.push_back(normal);
            
        } else if (strcmp(lineHeader, "f") == 0) {
            char *line;
            size_t len = 0;

            getline(&line, &len, file);

            std::string str(line);
            str.erase(0, 1);
            std::vector<std::string> tokens, tokensFaces;

            // Tokenize to get for instance 1/1/1, 2/6/4, ...
            tokenize(line, ' ', tokens);

            tokens.erase(tokens.begin());
            int firstVertexFace[3];
            int lastVertexFace[3];

            for (size_t i = 0; i < tokens.size(); i++) {
                tokenize(tokens.at(i), '/', tokensFaces);

                size_t len = tokensFaces.size();

                for (size_t j = 0; j < len; j++) {
                    if (!tokensFaces.at(j).empty()){
                        if (i < 3) {

                            if (i == 0) {
                                firstVertexFace[j] = std::stoi(tokensFaces.at(j));
                            }
                            if (i == 2) {
                                lastVertexFace[j] = std::stoi(tokensFaces.at(j));
                            }

                            if (j == 0) {
                                vertexIndices.push_back(std::stoi(tokensFaces.at(0)));
                            } else if (j == 1) {
                                uvIndices.push_back(std::stoi(tokensFaces.at(1)));
                            } else {
                                normalIndices.push_back(std::stoi(tokensFaces.at(2)));
                            }
                        } else {
                            // Triangulation
                            if (j == 0) {
                                vertexIndices.push_back(firstVertexFace[0]);
                                vertexIndices.push_back(lastVertexFace[0]);
                                vertexIndices.push_back(std::stoi(tokensFaces.at(0)));
                            } else if (j == 1) {
                                uvIndices.push_back(firstVertexFace[1]);
                                uvIndices.push_back(lastVertexFace[1]);
                                uvIndices.push_back(std::stoi(tokensFaces.at(1)));
                            } else {
                                normalIndices.push_back(firstVertexFace[2]);
                                normalIndices.push_back(lastVertexFace[2]);
                                normalIndices.push_back(std::stoi(tokensFaces.at(2)));
                            }
                            lastVertexFace[j] = std::stoi(tokensFaces.at(j));
                        }
                    }
                }
                tokensFaces.clear();

            }
        }
    }
    glm::vec3 vertex, vertex2, vertex3;

    maxPoint *= 2;

    if (maxPoint < 1) {
        maxPoint = 1/maxPoint;
    }

    for (unsigned int i=0; i < vertexIndices.size(); i++) {
        object->getOutIndices().push_back(vertexIndices[i]-1);
    }

    for (unsigned int i=0; i < temp_vertices.size(); i++) {
        vertex = temp_vertices[i];
        vertex.x /= maxPoint;
        vertex.y /= maxPoint;
        vertex.z /= maxPoint;
        object->getOutVertices().push_back(vertex);
    }

    unsigned int vs = object->getOutIndices().size();
    unsigned int s = object->getOutVertices().size();
    glm::vec3 faceNormal;
    glm::vec3 adjacentFacePerVertex[s];

    for (unsigned int i = 0; i < s; i++) {
        adjacentFacePerVertex[i] = glm::vec3(0.0f, 0.0f, 0.0f);
    }

    // Computes vertex normal
    for (unsigned int i = 0; i < vs; i+=3) {
        vertex = object->getOutVertices()[object->getOutIndices()[i]];
        vertex2 = object->getOutVertices()[object->getOutIndices()[i + 1]];
        vertex3 = object->getOutVertices()[object->getOutIndices()[i + 2]];

        faceNormal = glm::cross(vertex2-vertex, vertex3-vertex);

        adjacentFacePerVertex[object->getOutIndices()[i]] += faceNormal;
        adjacentFacePerVertex[object->getOutIndices()[i + 1]] += faceNormal;
        adjacentFacePerVertex[object->getOutIndices()[i + 2]] += faceNormal;
    }

    for(unsigned int i = 0; i < s; i++) {
        object->getOutNormals().push_back(normalize(adjacentFacePerVertex[i]));
    }

    fclose(file);
    return true;
}

void ObjReader::tokenize(std::string const &str, const char delimiter, std::vector<std::string> &out)
{
    // construct a stream from the string
    std::stringstream ss(str);
 
    std::string s;
    while (std::getline(ss, s, delimiter)) {
        out.push_back(s);
    }
}

Object* ObjReader::getObject()
{
    return object;
}
