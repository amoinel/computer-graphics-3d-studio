#include <iostream>

#include <glm/vec3.hpp> // glm::vec3
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/gtc/type_ptr.hpp>

// Class for camera specification
class Camera
{
public:
    Camera();
    ~Camera();

    /**
    * Returns the position of the camera.
    * @return The position of the camera.
    */
    glm::vec3 getPosition();

    /**
    * Returns the target, what the camera is looking.
    * @return The target of the camera.
    */
    glm::vec3 getTarget();

    /**
    * Returns the world upward.
    * @return The world upward.
    */
    glm::vec3 getUp();

    /**
    * Returns the view matrix.
    * @return The view matrix.
    */
    glm::mat4 getViewMatrix();


    // Disposition of the camera

    /**
    * Sets the position of the camera.
    * @param position The position of the camera.
    */
    void setPosition(glm::vec3 position);

    /**
    * Sets the target of the camera, what the camera is looking.
    * @param target The target of the camera.
    */
    void setTarget(glm::vec3 target);

    /**
    * Sets the world upward.
    * @param up The world upward.
    */
    void setUp(glm::vec3 up);

    /**
    * Sets the view matrix
    * @param viewMatrix The view matrix of the camera.
    */
    void setViewMatrix(glm::mat4 viewMatrix);

    /**
    * Updates the view matrix using glm lookAtRH.
    */
    void updateViewMatrix();

    // Actions on the camera

    /**
    * Rotates the view matrix.
    *
    * @param theta Degree of rotation.
    * @param x
    * @param y
    * @param z
    */
    void rotateCamera(float theta, const float x, const float y, const float z);

    /**
    * Translates the view matrix.
    *
    * @param dx Unit(s) of translation on the x axis.
    * @param dy Unit(s) of translation on the y axis.
    * @param dz Unit(s) of translation on the z axis.
    */
    void translateCamera(float dx, float dy, float dz);

private:
    glm::vec3 position;
    glm::vec3 target;
    glm::vec3 up;
    glm::mat4 matView;
};