#version 330
// Input vertex data, different for all executions of this shader.
in vec3 vPosition;
in vec3 vNormal;
in vec2 TexCoord;


uniform mat4 P;
uniform mat4 V;
uniform mat4 M;

out vec4 fN; // Normal vector
out vec2 f_texcoord;
out vec4 vPositionW;

void
main()
{
    fN = inverse(transpose(M)) * vec4(vNormal, 0.0f);
    vPositionW = M * vec4(vPosition, 1.0f);
    
    f_texcoord = TexCoord;
    
    gl_Position = P * V * M * vec4(vPosition, 1.0);
}
