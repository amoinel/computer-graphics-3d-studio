/*
 *  Geometry render based on Workshop 1
 *  Computer Graphics course
 *  Dept Computing Science, Umea University
 *  Stefan Johansson, stefanj@cs.umu.se
 *  Aurélien Moinel
 */
#pragma once

#include <glm/gtc/matrix_transform.hpp>
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4
#include <glm/mat4x4.hpp> // glm::mat4
#include "openglwindow.h"
#include "camera.h"
#include "projection.h"


class GeometryRender : public OpenGLWindow
{
public:
    template<typename... ARGS>
    GeometryRender(ARGS&&... args) : OpenGLWindow{ std::forward<ARGS>(args)... }
    {}

    /**
    * Initializes OpenGL
    *
    */
    void initialize();

    /**
    * Translates the model matrix.
    *
    * @param dx Unit(s) of translation on the x axis.
    * @param dy Unit(s) of translation on the y axis.
    * @param dz Unit(s) of translation on the z axis.
    */
    virtual void _translate(float dx, float dy, float dz) override;

    /**
    * Scales the model matrix.
    *
    * @param x Ratio of scale on the x axis.
    * @param y Ratio of scale on the y axis.
    * @param z Ratio of scale on the z axis.
    */
    virtual void _scale(float x, float y, float z) override;

    /**
    * Rotates the model matrix.
    *
    * @param theta Degree of rotation.
    * @param x
    * @param y
    * @param z
    */
    virtual void _rotate(float theta, float x, float y, float z) override;

    /**
    * Translates the view matrix.
    *
    * @param dx Unit(s) of translation on the x axis.
    * @param dy Unit(s) of translation on the y axis.
    * @param dz Unit(s) of translation on the z axis.
    */
    virtual void _translateCamera(float dx, float dy, float dz) override;

    /**
    * Rotates the view matrix.
    *
    * @param theta Degree of rotation.
    * @param x
    * @param y
    * @param z
    */
    virtual void _rotateCamera(float theta, float x, float y, float z) override;

    /**
    * Modifies value of uniform variable of the MVP matrix.
    *
    */
    virtual void reloadMatrix(void) override;

    /**
    * Reads an OBJ file in order to load it.
    * @param reader instance of the object reqder file.
    */
    virtual void loadFileGeometry(ObjReader &reader) override;

    /**
    * Updates the perspective projection when values are updated from th GUI.
    * @param fov Field of view.
    * @param far Far cliping.
    */
    virtual void updateProjectionPerspective(const float fov, const float far) override;

    /**
    * Updates the parallel projection when values are updated from th GUI.
    * @param top
    * @param farplane
    * @param obliqueScale
    * @param obliqueAngleRad
    */
    virtual void updateProjectionParallel(const float top, 
                                             const float farplane, 
                                             const float obliqueScale, 
                                             const float obliqueAngleRad) override;

    /**
    * Updates the material coefficient when values are updated from th GUI.
    * @param materialAmbient
    * @param materialDiffuse
    * @param materialSpecular
    * @param materialShininess
    */
    virtual void updateMaterialCoefficient(const float* materialAmbient, 
                                           const float* materialDiffuse, 
                                           const float* materialSpecular, 
                                           float materialShininess) override;


    /**
    * Updates the light coefficient when values are updated from th GUI.
    * @param lightPos
    * @param lightColor
    * @param ambientColor
    */
    virtual void updateLightCoefficient(const float* lightPos, const float* lightColor, const float* ambientColor) override;
    
    /**
    * Loads the texture in memory when chose from the GUI.
    * @param texturePath The file path of the texture.
    */
    virtual void loadTexture(const char* texturePath) override;

    /**
    * Applies or not the texture load in memory when chosen.
    * @param textureShow A boolean use to indicate if the texture must be apply or not.
    */
    virtual void applyTexture(const bool textureShow) override;

    /**
    * Displays the different objects on the scene.
    */
    virtual void display() override;

    /**
    * Updates the value of the height nd width of the window.
    * @param height The height of the window.
    * @param width The width of the window.
    */
    virtual void updateHeightWidth(const int height, const int width) override;

private:
    /**
    * Loads object data to the array buffer and index array and set the pointers 
    * of locVertices and locNormals to the right places.
    *
    */
    void setGlBuffers(void);

    /**
    * Creates Buffer for textures, wrapping, filtering and generates mipmaps.
    *
    */
    void initializeTexture(void);

    GLuint program;
    GLuint idTexture;
    GLuint locModel;
    GLuint locView;
    GLuint locProjection;
    GLuint locMVP;

    // OpenGL buffers
    GLuint vao;
    GLuint vbos[4];
    GLuint vBuffer;
    GLuint nBuffer;
    GLuint iBuffer;
    GLuint uvBuffer;

    // OpenGL attribute locations
    GLuint locVertices;
    GLuint locTex;
    GLuint locNormals;
    GLuint locUVs;
    GLuint locApplyTex;
    
    // OpenGL attribute locations light
    GLuint locAmbientR;
    GLuint locDiffuseR;
    GLuint locSpecularR;
    GLuint locShininess;

    GLuint locViewerPos;
    GLuint locLightPos;
    GLuint locLightColor;
    GLuint locAmbientColor;

    // Geometry data
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> UVvertices;
    std::vector<glm::vec2> texCoords;
    std::vector<unsigned int> indices;

    // Camera
    Camera camera;
    int heightWindow = 3;
    int widthWindow = 4;

    // Projection
    Projection projection;

    void debugShader(void) const;
    void loadGeometry(void);

    glm::mat4 matModel = glm::mat4(1.0f);
    glm::mat4 matView = camera.getViewMatrix();
    glm::mat4 matProjection = projection.getProjectionMatrix();

    glm::vec3 lAmbient = glm::vec3(.5f, .5f, .5f);
    glm::vec3 lDiffuse = glm::vec3(.5f, .5f, .5f);
    glm::vec3 lSpecular = glm::vec3(.5f, .5f, .5f);
    float lShininess = 1.0f;
    float lApplyTex = 0.0f;

    glm::vec3 lViewerPos = glm::vec3(0.0f, 0.0f, 2.0f);
    glm::vec3 lLightPos = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::vec3 lLightColor = glm::vec3(1.0f, 1.0f, 1.0f);
    glm::vec3 lAmbientColor = glm::vec3(0.2f, 0.2f, 0.2f);
};
