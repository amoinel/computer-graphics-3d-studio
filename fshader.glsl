#version 330
#extension GL_NV_shadow_samplers_cube : enable

out vec4  fColor;
//varying vec4 ptexture; 

// Outputs that are interpolated per fragment
in vec4 fN;
in vec2 f_texcoord;


uniform vec3 ambientR; //ka
uniform vec3 diffuseR; //kd
uniform vec3 specularR; //ks
uniform float shininess;
uniform float applyTex;

uniform vec3 lightColor;
uniform vec3 ambientColor;

uniform vec3 lightPos;
uniform vec3 viewerPos;

in vec4 vPositionW;

uniform sampler2D tex;
uniform samplerCube texSkyBox;

void
main()
{
    vec4 fNW = normalize(fN);
    vec4 VP = vec4(viewerPos, 1.0f) - vPositionW;
    vec4 LP = vec4(lightPos, 1.0f) - vPositionW;
    
    vec4 fViewerPos = normalize(VP);
    vec4 fLightPos = normalize(LP);

    float lambertian = max(dot(fNW, fLightPos), 0);
    vec3 ambient = ambientColor * ambientR; //Iambient
    vec3 diffuse = lightColor * diffuseR * lambertian; //Idiffuse
    vec3 specular = vec3(0.0f, 0.0f, 0.0f); //Ispecular

    if( dot(fNW, fLightPos) > 0 ) {
        vec4 reflectionDir = normalize(2 * dot(fNW, fLightPos) * fNW - fLightPos);
        specular = lightColor * specularR * pow(max(dot(reflectionDir, fViewerPos), 0), shininess);
    }
    if (applyTex == 1.0f) {
        fColor = vec4(ambient + diffuse + specular, 0.0f) * texture2D(tex, f_texcoord);
    } else {
        fColor = vec4(ambient + diffuse + specular, 0.0f);
    }

}
