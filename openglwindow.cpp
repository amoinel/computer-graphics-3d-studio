/*
 *  Workshop 1
 *  Computer Graphics course
 *  Dept Computing Science, Umea University
 *  Stefan Johansson, stefanj@cs.umu.se
 */
#include "openglwindow.h"
#include <imgui_internal.h>
#include <glm/gtc/type_ptr.hpp>

using namespace std;

// Initialize OpenGL context and viewport.
OpenGLWindow::OpenGLWindow(string title, int width, int height)
{
    // Initialize glfw
    if (!glfwInit())
        exit(EXIT_FAILURE);

    // Set minimum supported OpenGL version and OpenGL profile
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    // Create OpenGL window
    windowWidth = width;
    windowHeight = height;
    
    fov = 60.0f;
    farplane = 500.0f;
    top = 1.0f;
    obliqueScale = 0.0f;
    obliqueAngleRad = pi_f/4.0f;
    
    glfwWindow = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);
    if (glfwWindow == nullptr) {
        glfwTerminate();
        cerr << "Could not open window or initialize OpenGL context." << endl;
        exit(EXIT_FAILURE);
    }

    glfwMakeContextCurrent(glfwWindow);
    glfwSwapInterval(1); 
    
    // Initialize glew
    glewExperimental = GL_TRUE;
    GLenum glewError = glewInit();
    if (glewError != GLEW_OK) {
        cout << "glew init error:" << endl << glewGetErrorString(glewError) << endl;
        exit(EXIT_FAILURE);
    }

    if ( !GLEW_VERSION_4_3 ) {
        cout << "Warning: OpenGL 4.3+ not supported by the GPU!" << endl;
        cout << "Decreace supported OpenGL version if needed." << endl;
    }

    // Create Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    // Set Platform/Renderer backends
    ImGui_ImplGlfw_InitForOpenGL(glfwWindow, true);
    ImGui_ImplOpenGL3_Init(NULL);

    // Set graphics attributes
    glPointSize(5.0); // Unsupported in OpenGL ES 2.0 (Qt on MS Windows)
    glLineWidth(1.0);
    glClearColor(0.0, 0.0, 0.0, 0.0);

    glViewport(0, 0, width, height);
}

OpenGLWindow::~OpenGLWindow()
{
    // Cleanup ImGui
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwDestroyWindow(glfwWindow);
    glfwTerminate();
}

// OpenGL error handler
bool 
OpenGLWindow::checkOpenGLError() const
{
    bool foundError = false;
    GLenum glError = glGetError();
    
    while (glError != GL_NO_ERROR) {
        cerr << "glError: " << glError << endl;
        foundError = true;
        glError = glGetError();
    }
    return foundError;
}

int 
OpenGLWindow::width() const
{
    return windowWidth;
}

int 
OpenGLWindow::height() const
{
    return windowHeight;
}

GLFWwindow* 
OpenGLWindow::window() const
{
    return glfwWindow;
}

// Read shader source files
string
OpenGLWindow::readShaderSource(const string shaderFile) const
{
    string shaderSource;
    string line;

    fstream fs(shaderFile, ios::in);
    if(!fs)
        return shaderSource;

    while (!fs.eof()) {
        getline(fs, line);
        shaderSource.append(line + '\n');
    }
    fs.close();
    return shaderSource;
}


// Initialize OpenGL shader program
GLuint 
OpenGLWindow::initProgram(const string vShaderFile, const string fShaderFile) const
{
    GLuint program;
    int i;
    GLint  linked;

    struct shaders_t{
        string   filename;
        GLenum   type;
    };

    shaders_t shaders[2] = {
        { vShaderFile, GL_VERTEX_SHADER },
        { fShaderFile, GL_FRAGMENT_SHADER }
    };

    program = glCreateProgram();
    for (i = 0; i < 2; ++i ) {
        GLuint shader;
        GLint  compiled;

        string shaderSource = readShaderSource( shaders[i].filename );
        if ( shaderSource.empty() ) {
            cerr << "Failed to read " << shaders[i].filename << endl;
            exit( EXIT_FAILURE );
        }

        shader = glCreateShader( shaders[i].type );
        const char *shaderSrc = shaderSource.c_str();
        glShaderSource( shader, 1, &shaderSrc, NULL );
        glCompileShader( shader );
        checkOpenGLError();

        glGetShaderiv( shader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLint  logSize;

            cerr << "Failed to compile " << shaders[i].filename << endl;
            glGetShaderiv( shader, GL_INFO_LOG_LENGTH, &logSize );
            if (logSize > 0) {
                char logMsg[logSize+1];
                glGetShaderInfoLog( shader, logSize, nullptr, &(logMsg[0]) );
                cerr << "Shader info log: " << logMsg << endl;
            }
            exit( EXIT_FAILURE );
        }
        glAttachShader( program, shader );
    }

    /* link  and error check */
    glLinkProgram(program);
    checkOpenGLError();

    glGetProgramiv( program, GL_LINK_STATUS, &linked );
    if ( !linked ) {
        GLint  logSize;

        cerr << "Shader program failed to link!" << endl;

        glGetProgramiv( program, GL_INFO_LOG_LENGTH, &logSize);
        if ( logSize > 0 ) {
            char logMsg[logSize+1];
            glGetProgramInfoLog( program, logSize, NULL, &(logMsg[0]) );
            cerr << "Program info log: " << logMsg << endl;
            free(logMsg);
        }
        exit( EXIT_FAILURE );
    }

    return program;
}


// The window resize callback function
void 
OpenGLWindow::resizeCallback(GLFWwindow* window, int width, int height)
{
    reshape(width, height);
}

// The window resize rotate function
void 
OpenGLWindow::keysCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS || action == GLFW_REPEAT) {
        if (key == GLFW_KEY_L) {
            _translate(0.1f, 0.0f, 0.0f);
        } else if (key == GLFW_KEY_J) {
            _translate(-0.1f, 0.0f, 0.0f);
        } else if (key == GLFW_KEY_I) {
            _translate(0.0f, 0.1f, 0.0f);
        } else if (key == GLFW_KEY_K) {
            _translate(0.0f, -0.1f, 0.0f);
        } else if (key == GLFW_KEY_P) {
            _scale(1.1f, 1.1f, 1.1f);
        } else if (key == GLFW_KEY_M) {
            _scale(0.9f, 0.9f, 0.9f);
        } 
        // Checks if the ctrl key is activated (rotation on z axis)
        else if (key == GLFW_KEY_UP && mods == GLFW_MOD_CONTROL) {
            _rotate(10, 0.0f, 0.0f, 1.0f);
        } else if (key == GLFW_KEY_DOWN && mods == GLFW_MOD_CONTROL) {
            _rotate(-10, 0.0f, 0.0f, 1.0f);
        } 
        
        else if (key == GLFW_KEY_UP) {
            _rotate(10, 1.0f, 0.0f, 0.0f);
        } else if (key == GLFW_KEY_DOWN) {
            _rotate(-10, 1.0f, 0.0f, 0.0f);
        } else if (key == GLFW_KEY_RIGHT) {
            _rotate(10, 0.0f, 1.0f, 0.0f);
        } else if (key == GLFW_KEY_LEFT) {
            _rotate(-10, 0.0f, 1.0f, 0.0f);
        } else if (key == GLFW_KEY_E) {
            _translateCamera(0.1f, 0.0f, 0.0f);
        } else if (key == GLFW_KEY_Q) {
            _translateCamera(-0.1f, 0.0f, 0.0f);
        } else if (key == GLFW_KEY_D) {
            _translateCamera(0.0f, 0.1f, 0.0f);
        } else if (key == GLFW_KEY_A) {
            _translateCamera(0.0f, -0.1f, 0.0f);
        } else if (key == GLFW_KEY_W) {
            _translateCamera(0.0f, 0.0f, -0.1f);
        } else if (key == GLFW_KEY_S) {
            _translateCamera(0.0f, 0.0f, 0.1f);
        }
    }
}

void 
OpenGLWindow::mousePosCallback(GLFWwindow* window, double xpos, double ypos)
{

    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_MIDDLE)) {
        float angle = 1.0f;

        if (xpos < lastXpos) {
            _rotateCamera(angle, 0.0f, 1.0f, 0.0f);
            horizontalAngle += angle;
        } else if (xpos > lastXpos) {
            _rotateCamera(-angle, 0.0f, 1.0f, 0.0f);
            horizontalAngle -= angle;
        }
        if (ypos < lastYpos) {
            _rotateCamera(angle, 1.0f, 0.0f, 0.0f);
            verticalAngle += angle;
        } else if (ypos > lastYpos) {
            _rotateCamera(-angle, 1.0f, 0.0f, 0.0f);
            verticalAngle -= angle;
        }
        lastXpos = xpos;
        lastYpos = ypos;
    }
    
}

// GLFW error callback function
void 
OpenGLWindow::errorCallback(int error, const char* description)
{
    cerr << "GLFW error: " << description << endl;
}

void
OpenGLWindow::DrawGui()
{

    IM_ASSERT(ImGui::GetCurrentContext() != NULL && "Missing dear imgui context.");

    static ImGuiSliderFlags flags = ImGuiSliderFlags_AlwaysClamp;
    
    ImGui::Begin("3D Studio");

    if (ImGui::CollapsingHeader("OBJ File")) {
        ImGui::Text("OBJ file: %s", objFileName.c_str());
        if (ImGui::Button("Open File"))
            igfd::ImGuiFileDialog::Instance()->OpenDialog("ChooseFileDlgKey", "Choose File", ".obj", ".");
        
        if (igfd::ImGuiFileDialog::Instance()->FileDialog("ChooseFileDlgKey")) {
            if (igfd::ImGuiFileDialog::Instance()->IsOk == true) {
                objFileName = igfd::ImGuiFileDialog::Instance()->GetCurrentFileName();
                objFilePath = igfd::ImGuiFileDialog::Instance()->GetCurrentPath();
                objFilePath += "/" + objFileName;
                std::cout << objFilePath << std::endl;
                Object* obj = new Object();
                ObjReader reader(objFilePath.c_str(), obj);
                loadFileGeometry(reader);
            }
            // close
            igfd::ImGuiFileDialog::Instance()->CloseDialog("ChooseFileDlgKey");
        }
    }

    if (ImGui::CollapsingHeader("Light")) {
        ImGui::Text("Light source position");
        ImGui::PushItemWidth(100);
        ImGui::InputFloat("x", &lightPos[0], 0.5f, 1.0f, "%1.1f"); ImGui::SameLine();
        ImGui::InputFloat("y", &lightPos[1], 0.5f, 1.0f, "%1.1f"); ImGui::SameLine();
        ImGui::InputFloat("z", &lightPos[2], 0.5f, 1.0f, "%1.1f");
        ImGui::PopItemWidth();

        ImGui::Text("Light source intensity:");
        ImGui::ColorEdit3("Light", lightColor);
        
        ImGui::Text("Ambient light intensity:");
        ImGui::ColorEdit3("Ambient", ambientColor);

        updateLightCoefficient(lightPos, lightColor, ambientColor);
    }
    
    if (ImGui::CollapsingHeader("Object Material")) {
        ImGui::Text("Ambient coefficient:");
        ImGui::ColorEdit3("Ambient color", materialAmbient);
        
        ImGui::Text("Diffuse coefficient:");
        ImGui::ColorEdit3("Diffuse color", materialDiffuse);
        
        ImGui::Text("Specular coefficient:");
        ImGui::ColorEdit3("Specular color", materialSpecular);

        ImGui::SliderFloat("Shininess", &materialShininess, 1.0f, 1000.0f, "%1.0f", flags);
        updateMaterialCoefficient(materialAmbient, materialDiffuse, materialSpecular, materialShininess);
    }
    
    if (ImGui::CollapsingHeader("Object Texture")) {
        ImGui::Checkbox("Show texture", &textureShow);
        applyTexture(textureShow);
        ImGui::Text("Texture file: %s", textureFileName.c_str());
        if (ImGui::Button("Open Texture File"))
            igfd::ImGuiFileDialog::Instance()->OpenDialog("ChooseFileTexDlgKey", "Select Texture File",
                                                          ".jpg,.bmp,.dds,.hdr,.pic,.png,.psd,.tga", ".");
        
        if (igfd::ImGuiFileDialog::Instance()->FileDialog("ChooseFileTexDlgKey")) {
            if (igfd::ImGuiFileDialog::Instance()->IsOk == true) {
                textureFileName = igfd::ImGuiFileDialog::Instance()->GetCurrentFileName();
                textureFilePath = igfd::ImGuiFileDialog::Instance()->GetCurrentPath();
                cout << "Texture file: " << textureFileName << endl << "Path: " << textureFilePath << endl;
                textureFilePath += "/" + textureFileName;
                loadTexture(textureFilePath.c_str());
            } else {
                std::cout << "File cannot be opened" << std::endl;
            }
            // close
            igfd::ImGuiFileDialog::Instance()->CloseDialog("ChooseFileTexDlgKey");
        }
    }
    
    if (ImGui::CollapsingHeader("Projection")) {
        const char* items[] = {"Perspective", "Parallel" };
        // proj_current_idx = 0;
        if (ImGui::Combo("projektion", &proj_current_idx, items, IM_ARRAYSIZE(items), IM_ARRAYSIZE(items)));
        if (proj_current_idx == 0) {
            ImGui::SliderFloat("Field of view",&fov, 20.0f, 160.0f, "%1.0f", flags);
            ImGui::SliderFloat("Far",&farplane, 1.0f, 1000.0f, "%1.0f", flags);
            updateProjectionPerspective(fov, farplane);
        }
        if (proj_current_idx == 1) {
            ImGui::SliderFloat("Top",&top, 1.0f, 100.0f, "%.1f", flags);
            ImGui::SliderFloat("Far",&farplane, 1.0f, 1000.0f, "%1.0f", flags);
            ImGui::SliderFloat("Oblique scale",&obliqueScale, 0.0f, 1.0f, "%.1f", flags);
            ImGui::SliderAngle("Oblique angle",&obliqueAngleRad, 15, 75, "%1.0f", flags);
            updateProjectionParallel(top, farplane, obliqueScale, obliqueAngleRad);
        }
    }

    ImGui::End();
}

// Start the GLFW loop
void 
OpenGLWindow::start()
{
    // Loop until the user closes the window
    while (!glfwWindowShouldClose(glfwWindow)) {
        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        // ImGui example gui
        //ImGui::ShowDemoWindow(&show_demo_window);

        // Draw the gui
        DrawGui();

        // Call display in geomentryRender to render the scene
        display();

        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
        
        // Swap buffers
        glfwSwapBuffers(glfwWindow);

        // Sleep and wait for an event
        glfwWaitEvents();
    }
    
}

// Render the scene 
void OpenGLWindow::displayNow()
{
    if (glfwGetCurrentContext() == nullptr)
        return;

    display();
}

void OpenGLWindow::reshape(const int width, const int height)
{
    if (glfwGetCurrentContext() == nullptr)
        return;
    glViewport(0, 0, width, height);
    updateHeightWidth(height, width);
    if (proj_current_idx == 0) {
        updateProjectionPerspective(fov, farplane);
    } else {
        updateProjectionParallel(top, farplane, obliqueScale, obliqueAngleRad);
    }
}


